# Doodlebug for Grasshopper #

A plug-in for GH to connect Grasshopper to Illustrator

### How do I get set up? ###

Clone to your hard drive and launch the .sln in visual studio. You may need to re-point the references for RhinoCommon and Grasshopper in the plug-in project, and you may also need to re-register the COM interface reference in the IllustratorInterface project - delete Illustrator from the references, right-click, "Add reference," go to COM, and locate the Adobe Illustrator Type Library and add it. I have been leaving this reference set to "Embed Interop Types" in the Reference Properties. 